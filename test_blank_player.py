import pytest
from itertools import permutations, combinations, combinations_with_replacement

class Player:
    def __init__(self, number = 0, candidates=None):
        self.number = number
        if candidates is None:
            candidates = self.new_candidates()
        self._candidates = candidates

    # #@staticmethod
    # def old_candidates():
    #     return [str(el).zfill(4) for el in range(0, 14)]

    @staticmethod
    def new_candidates():
        return ["".join(el) for el in permutations("0123456789", 4)]

    def guess(self):
        """Generate a random four-digit string guess for the Mastermind API"""
        
        return self._candidates[self.number]

    def process_results(self, current_guess, correct_digits, misplaced_digits):
        """Do something useful with the results of the current guess"""
        # TODO: More useful than this, maybe?
        magic_filter = MagicFilter(current_guess, correct_digits, misplaced_digits)

        next_candidates = [ el for el in self._candidates if magic_filter.is_valid_candidate(el)]
        self._candidates = next_candidates
    
class MagicFilter:
    def __init__(self, current_guess, correct_digits, misplaced_digits):
        self._current_guess = current_guess
        self._correct_digits = correct_digits
        self._misplaced_digits = misplaced_digits

    def is_valid_candidate(self, el):
        return not set(self._current_guess).intersection(set(el))


def test_magic_filter():
    magic_filter = MagicFilter("0123", 0, 0)
    assert not magic_filter.is_valid_candidate("3210")

    magic_filter = MagicFilter("0123", 0, 1)
    assert magic_filter.is_valid_candidate("7890")

# def test_abcd():
#     player = Player(12)
#     assert player.guess() == "0012"
#     #assert Player().guess()
#     player.process_results(None,None,None)
#     assert player.guess() == "0013"

# def test_initialized_with_null_constructor():

#     player = Player()
#     assert player.guess() == "0001"
#     player.process_results(None,None,None)
#     assert player.guess() == "0002"

def test_new_candidate():
    new_candidates = Player.new_candidates()

    assert 5040 == len(new_candidates)
    assert 5040  == len(set(new_candidates))
    assert all(isinstance(el, str) for el in new_candidates)
    assert all(len(set(el)) == 4 for el in new_candidates)


def test_player_with_new_candidates():
    player = Player(candidates = Player.new_candidates())
    assert player.guess() == "0123"
    player.process_results("0123", 0, 0)
    
    second_guess = player.guess()
    assert "0" not in second_guess
    assert "1" not in second_guess
    assert "2" not in second_guess
    assert "3" not in second_guess


if __name__ == "__main__":
    player = Player()
    random_guess = player.guess()
    print(f"This Player provides random guesses: {random_guess}")
    print("Run mastermind_example_client.py to use it in the challenge!")

    